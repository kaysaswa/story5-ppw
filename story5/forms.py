from django.forms import ModelForm
from django import forms
from .models import PostCourse

class CourseForm(forms.ModelForm):
    class Meta:
        model = PostCourse
        fields = [
            'Course',
            'Lecturer',
            'Credit',
            'Semester',
            'Description'
        ]

        widgets = {
            'Course' : forms.TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Enter Course',
                    'style':'#FF0C55',
                    'id':'Course',
                    'name':'Course',
                    
                }
            ),
            'Lecturer' : forms.TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Enter Lecturer',
                    'style':'#FF0C55',
                    'id':'Lecturer',
                    'name':'Lecturer',
                    
                }
            ),
            'Credit' : forms.Select(
                attrs={
                    'class':'form-control',
                    'placeholder':'Enter Course',
                    'style':'#FF0C55',
                    'id':'Credit',
                    'name':'Credit',
                    
                }
            ),
            'Semester' : forms.Select(
                attrs={
                    'class':'form-control',
                    'placeholder':'Enter Course',
                    'style':'#FF0C55',
                    'id':'Semester',
                    'name':'Semester',
                    
                }
            ),
            'Description' : forms.Textarea(
                attrs={
                    'class':'form-control',
                    'placeholder':'Enter Course Description',
                    'style':'#FF0C55',
                    'id':'Description',
                    'name':'Description',
                    
                }
            )
        }