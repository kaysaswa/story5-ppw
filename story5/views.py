from django.shortcuts import render,redirect
from .forms import CourseForm
from .models import PostCourse
from django.views.generic import ListView,DetailView,CreateView,DeleteView

# Create your views here.
class courses(ListView):
    model = PostCourse
    template_name = 'story5s/courses.html'
    #page = 'COURSES'
    #model_course = PostCourse.objects.all()
    #model_course = model.objects.all()
    #context = {
     
     
     #  'title' : page,
      #  'course' : model_course
    #}

    #return render(request,'story5s/COURSES1.html',context)

def addcourse(request):
    model = PostCourse
    form = CourseForm(request.POST or None)

    if request.POST and form.is_valid():
        form.save()
        
    context = {
        'form' : form
        }

    return render(request,'story5s/addcourse.html',context)  


class detailcourse(DetailView):
    model = PostCourse
    template_name = 'story5s/detailcourse.html'

def deletecourse(request,delete_id):
    PostCourse.objects.filter(id=delete_id).delete()
    return redirect('story5:courses')