from django.db import models
from django.urls import reverse

# Create your models here.
Credit_Choices=[
    ('one','1'),
    ('two','2'),
    ('three','3'),
    ('four','4'),
    ('five','5'),
    ('six','6'),
    ('seven','7')
]

Semester_Choices=[
    ('Gasal','Gasal 2020/2021')
]
class PostCourse(models.Model):
    Course = models.CharField(max_length=20)
    Lecturer = models.CharField(max_length=20)
    Credit = models.CharField(max_length=20, choices=Credit_Choices,default='1')
    Semester = models.CharField(max_length=20, choices=Semester_Choices,default='Gasal 2020/2021')
    Description = models.TextField()

    published = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.Course
    
 
