from django.urls import path

from . import views
from .views import courses,addcourse,detailcourse,deletecourse

app_name = 'story5'

urlpatterns = [
    path('', courses.as_view(), name='courses'),
    path('addcourse', views.addcourse, name='addcourse'),
    path('detailcourse/<pk>/',detailcourse.as_view(),name='detailcourse'),
    path('deletecourse/(?P<delete_id>[0-9])',views.deletecourse,name='deletecourse')
]